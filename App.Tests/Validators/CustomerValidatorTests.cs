﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using App.Validators;

namespace App.Tests.Validators
{
    [TestClass]
    public class CustomerValidatorTests
    {
        #region Name Tests

        [TestMethod]
        public void NameIsValid()
        {
            var validator = new CustomerValidator();

            var result = validator.IsNameValid("Bob", "Smith");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void NameIsInvalidOnNulls()
        {
            var validator = new CustomerValidator();

            var firstNameResult = validator.IsNameValid(null, "Smith");
            var surnameResult = validator.IsNameValid("Bob", null);

            Assert.IsFalse(firstNameResult);
            Assert.IsFalse(surnameResult);
        }

        [TestMethod]
        public void NameIsInvalidOnEmpty()
        {
            var validator = new CustomerValidator();

            var firstNameResult = validator.IsNameValid(string.Empty, "Smith");
            var surnameResult = validator.IsNameValid("Bob", string.Empty);

            Assert.IsFalse(firstNameResult);
            Assert.IsFalse(surnameResult);
        }

        #endregion

        #region Age Tests

        [TestMethod]
        public void AgeIsValid()
        {
            var validator = new CustomerValidator();
            var dateofBirth = DateTime.Now.AddYears(-21);

            var result = validator.IsAgeValid(dateofBirth);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void AgeIsInvalidInYears()
        {
            var validator = new CustomerValidator();
            var dateofBirth = DateTime.Now.AddYears(-20);

            var result = validator.IsAgeValid(dateofBirth);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AgeIsInvalidInDays()
        {
            var validator = new CustomerValidator();
            var dateofBirth = DateTime.Now.AddYears(-21).AddDays(1);

            var result = validator.IsAgeValid(dateofBirth);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AgeIsInvalidExtremes()
        {
            var validator = new CustomerValidator();
            var dateofBirth = DateTime.MaxValue;

            var result = validator.IsAgeValid(dateofBirth);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AgeIsValidExtremes()
        {
            var validator = new CustomerValidator();
            var dateofBirth = DateTime.MinValue;

            var result = validator.IsAgeValid(dateofBirth);

            Assert.IsTrue(result);
        }

        #endregion

        #region Email Tests

        [TestMethod]
        public void EmailIsValid()
        {
            var validator = new CustomerValidator();

            var result = validator.IsEmailValid("bob@test.com");

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void EmailIsInvalidForm()
        {
            var validator = new CustomerValidator();
            var invalidEmails = new List<string>
            {
                "bob",
                "bob@",
                "bob.com"
            };

            foreach (var email in invalidEmails)
            {
                var result = validator.IsEmailValid(email);

                Assert.IsFalse(result);
            }
        }

        #endregion

        #region Credit Tests

        [TestMethod]
        public void NoCreditLimitIsValid()
        {
            var validator = new CustomerValidator();

            var result = validator.IsValidCreditLimit(false, 0);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CreditLimitIsValid()
        {
            var validator = new CustomerValidator();

            var result = validator.IsValidCreditLimit(true, 500);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CreditLimitIsInvalidTooLow()
        {
            var validator = new CustomerValidator();

            var result = validator.IsValidCreditLimit(true, 499);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void CreditLimitIsInvalidNegative()
        {
            var validator = new CustomerValidator();

            var result = validator.IsValidCreditLimit(true, -500);

            Assert.IsFalse(result);
        }

        #endregion
    }
}
