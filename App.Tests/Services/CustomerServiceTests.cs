﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using App.Services;
using App.Data;
using App.Validators;
using App.Models;

namespace App.Tests.Services
{
    [TestClass]
    public class CustomerServiceTests
    {
        #region CreditLimitTests

        private ICustomerCreditService GetMockCreditService(int returnCredit)
        {
            var creditService = new Mock<ICustomerCreditService>();
            creditService.Setup(s => s.GetCreditLimit(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>())).Returns(returnCredit);

            return creditService.Object;
        } 

        [TestMethod]
        public void GoldCreditValid()
        {
            var returnCredit = 1000;
            var customerService = new CustomerService(new Mock<ICompanyRepository>().Object, new Mock<ICustomerValidator>().Object, GetMockCreditService(returnCredit));
            var customer = new Customer(null, null,DateTime.Now, null)
            {
                Company = new Company{ Classification = Classification.Gold }
            };

            int creditLimit;
            var result = customerService.CustomerHasCreditLimit(customer, out creditLimit);

            Assert.IsFalse(result);
            Assert.AreEqual(returnCredit, creditLimit);
        }

        [TestMethod]
        public void GoldCreditInvalid()
        {
            var returnCredit = 1000;
            var customerService = new CustomerService(new Mock<ICompanyRepository>().Object, new Mock<ICustomerValidator>().Object, GetMockCreditService(returnCredit));
            var customer = new Customer(null, null, DateTime.Now, null)
            {
                Company = new Company { Classification = Classification.Silver }
            };

            int creditLimit;
            var result = customerService.CustomerHasCreditLimit(customer, out creditLimit);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SilverCreditValid()
        {
            var returnCredit = 1000;
            var customerService = new CustomerService(new Mock<ICompanyRepository>().Object, new Mock<ICustomerValidator>().Object, GetMockCreditService(returnCredit));
            var customer = new Customer(null, null, DateTime.Now, null)
            {
                Company = new Company { Classification = Classification.Silver }
            };

            int creditLimit;
            var result = customerService.CustomerHasCreditLimit(customer, out creditLimit);

            Assert.IsTrue(result);
            Assert.AreEqual(returnCredit * 2, creditLimit);
        }

        [TestMethod]
        public void BronzeCreditValid()
        {
            var returnCredit = 1000;
            var customerService = new CustomerService(new Mock<ICompanyRepository>().Object, new Mock<ICustomerValidator>().Object, GetMockCreditService(returnCredit));
            var customer = new Customer(null, null, DateTime.Now, null)
            {
                Company = new Company { Classification = Classification.Bronze }
            };

            int creditLimit;
            var result = customerService.CustomerHasCreditLimit(customer, out creditLimit);

            Assert.IsTrue(result);
            Assert.AreEqual(returnCredit, creditLimit);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreditCustomerNull()
        {
            var returnCredit = 1000;
            var customerService = new CustomerService(new Mock<ICompanyRepository>().Object, new Mock<ICustomerValidator>().Object, GetMockCreditService(returnCredit));

            int creditLimit;
            var result = customerService.CustomerHasCreditLimit(null, out creditLimit);
        }

        #endregion
    }
}
