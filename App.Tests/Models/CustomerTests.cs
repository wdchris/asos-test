﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using App.Models;

namespace App.Tests.Models
{
    [TestClass]
    public class CustomerTests
    {
        [TestMethod]
        public void CustomerHasCreditLimitByDefault()
        {
            var customer = new Customer(null, null, DateTime.Now, null);

            Assert.IsTrue(customer.HasCreditLimit);
        }
    }
}
