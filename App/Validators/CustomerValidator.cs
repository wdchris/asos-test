﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Validators
{
    public interface ICustomerValidator
    {
        bool IsNameValid(string firstName, string surname);
        bool IsEmailValid(string email);
        bool IsAgeValid(DateTime dateOfBirth);
        bool IsValidCreditLimit(bool hasCreditLimit, int creditLimit);
    }

    public class CustomerValidator : ICustomerValidator
    {
        public int MinimumAge { get; set; }
        public int MinimumCreditLimit { get; set; }

        public CustomerValidator(int minimumAge = 21, int minimumCreditLimit = 500)
        {
            MinimumAge = minimumAge;
            MinimumCreditLimit = minimumCreditLimit;
        }

        /// <summary>
        /// Ensures a first name and surname are supplied
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="surname"></param>
        /// <returns></returns>
        public bool IsNameValid(string firstName, string surname)
        {
            return !string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(surname);
        }

        /// <summary>
        /// Ensures the email address is of valid form (contains @ and .)
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool IsEmailValid(string email)
        {
            return email.Contains("@") && email.Contains(".");
        }

        /// <summary>
        /// Ensures the supplied date is of minumum age for validator
        /// </summary>
        /// <param name="dateOfBirth"></param>
        /// <returns></returns>
        public bool IsAgeValid(DateTime dateOfBirth)
        {
            var now = DateTime.Now;
            int age = now.Year - dateOfBirth.Year;

            if (now.Month < dateOfBirth.Month || (now.Month == dateOfBirth.Month && now.Day < dateOfBirth.Day)) 
                age--;

            return age >= MinimumAge;
        }

        /// <summary>
        /// Ensures that if we have a credit limit, it's above the validator minimum
        /// </summary>
        /// <param name="hasCreditLimit"></param>
        /// <param name="creditLimit"></param>
        /// <returns></returns>
        public bool IsValidCreditLimit(bool hasCreditLimit, int creditLimit)
        {
            return !hasCreditLimit || creditLimit >= MinimumCreditLimit;
        }
    }
}
