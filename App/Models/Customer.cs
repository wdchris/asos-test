using System;

namespace App.Models
{
    public class Customer
    {
        public int Id { get; set; }

        public string Firstname { get; set; }

        public string Surname { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string EmailAddress { get; set; }

        public bool HasCreditLimit { get; set; }

        public int CreditLimit { get; set; }

        public Company Company { get; set; }

        public Customer(string firstName, string surname, DateTime dateofBirth, string emailAddress)
        {
            Firstname = firstName;
            Surname = surname;
            DateOfBirth = dateofBirth;
            EmailAddress = emailAddress;
            HasCreditLimit = true;
        }
    }
}