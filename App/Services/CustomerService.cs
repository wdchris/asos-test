﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.Data;
using App.Models;
using App.Validators;

namespace App.Services
{
    public class CustomerService
    {
        private ICompanyRepository _companyRepository;
        private ICustomerValidator _customerValidator;
        private ICustomerCreditService _customerCreditService;

        public CustomerService(ICompanyRepository companyRepository, ICustomerValidator customerValidator, ICustomerCreditService customerCreditService)
        {
            _companyRepository = companyRepository;
            _customerValidator = customerValidator;
            _customerCreditService = customerCreditService;
        }

        public bool AddCustomer(string firstName, string surname, string email, DateTime dateOfBirth, int companyId)
        {
            // check customer is valid
            if (!_customerValidator.IsNameValid(firstName, surname) ||
                !_customerValidator.IsEmailValid(email) ||
                !_customerValidator.IsAgeValid(dateOfBirth))
            {
                return false;
            }
            
            // lookup customer company
            var company = _companyRepository.GetById(companyId);
            var customer = new Customer(firstName, surname, dateOfBirth, email)
            {
                Company = company
            };

            // lookup and validate credit limit
            int creditLimit;
            var hasLimit = CustomerHasCreditLimit(customer, out creditLimit);
            if (!_customerValidator.IsValidCreditLimit(hasLimit, creditLimit))
            {
                return false;
            }
            else
            {
                customer.HasCreditLimit = hasLimit;
                customer.CreditLimit = creditLimit;
            }

            // add to the DB
            CustomerDataAccess.AddCustomer(customer);

            return true;
        }

        /// <summary>
        /// Lookup a customer credit rating based on the company
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="creditLimit"></param>
        /// <returns>Whether customer has a limit</returns>
        public bool CustomerHasCreditLimit(Customer customer, out int creditLimit)
        {
            if (customer == null)
                throw new ArgumentNullException("Customer must be supplied");

            creditLimit = _customerCreditService.GetCreditLimit(customer.Firstname, customer.Surname, customer.DateOfBirth);

            if (customer.Company != null)
            {
                switch (customer.Company.Classification)
                {
                    case Classification.Gold:
                        {
                            return false;
                        }
                    case Classification.Silver:
                        {
                            creditLimit *= 2;
                            return true;
                        }
                    case Classification.Bronze:
                        {
                            return true;
                        }
                }
            }

            return true;
        }
    }
}
